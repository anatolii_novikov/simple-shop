(function () {
    'use strict';

    function shopFunction() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'templates/modules/shop/directives/shop.html',
            scope: {
                goods: '='
            }
        };
    }

    angular.module('shop.directives').directive('shop', shopFunction);
}());