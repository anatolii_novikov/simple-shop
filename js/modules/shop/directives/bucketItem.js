(function () {
    'use strict';

    function bucketItemCtrl($scope) {
        function updateSum() {
            $scope.item.sum = ($scope.item.count * $scope.item.price);
        }
        $scope.countChange = function () {
            updateSum();
            $scope.$emit('bucketItem:countChange', $scope.item);
        };
        $scope.deleteItem = function () {
            $scope.$emit('bucketItem:deleteItem', $scope.item);
        };
    }

    function bucketItemFunction() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'templates/modules/shop/directives/bucket-item.html',
            scope: {
                item: '='
            },
            controller: ['$scope', bucketItemCtrl]
        };
    }

    angular.module('shop.directives').directive('bucketItem', bucketItemFunction);
}());