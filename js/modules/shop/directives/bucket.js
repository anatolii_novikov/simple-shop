(function () {
    'use strict';

    function bucketCtrl($scope) {
        function updateTotal(goods) {
            $scope.total = 0;
            goods.forEach(function (item) {
                $scope.total += item.sum;
            });
        }

        $scope.total = 0;
        $scope.goods = [];
        $scope.prepareItem = function (event, index, item) {
            return Object.create(item, {
                count: { writable: true, configurable: true, value: 1 },
                sum: { writable: true, configurable: true, value: item.price }
            });
        };
        $scope.$watch('goods', updateTotal, true);

        $scope.$on('bucketItem:countChange', function () {
            updateTotal($scope.goods);
        });

        $scope.$on('bucketItem:deleteItem', function (event, item) {
            $scope.goods.splice($scope.goods.indexOf(item), 1);
        });
    }

    function bucketFunction() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'templates/modules/shop/directives/bucket.html',
            scope: {},
            controller: ['$scope', bucketCtrl]
        };
    }

    angular.module('shop.directives').directive('bucket', bucketFunction);
}());