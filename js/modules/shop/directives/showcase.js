(function () {
    'use strict';

    function showcaseCtrl($scope) {
        $scope.deleteItem = function (item) {
            $scope.goods.splice($scope.goods.indexOf(item), 1);
        };
    }

    function showcaseFunction() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'templates/modules/shop/directives/showcase.html',
            scope: {
                goods: '='
            },
            controller: ['$scope', showcaseCtrl]
        };
    }

    angular.module('shop.directives').directive('showcase', showcaseFunction);
}());