(function () {
    'use strict';
    angular.module('shop.directives', []);
    angular.module('shop', [
        'dndLists',
        'shop.directives'
    ]);
}());