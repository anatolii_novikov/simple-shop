(function () {
    'use strict';
    angular.module('mainApplication.services', []);
    angular.module('mainApplication.controllers', []);
    angular.module('mainApplication', [
        'mainApplication.services',
        'mainApplication.controllers'
    ]);
}());