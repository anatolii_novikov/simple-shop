(function () {
    'use strict';

    function apiFunction() {
        return {
            getGoods: function () {
                return [
                    {
                        title: 'Juice',
                        price: 1.2
                    },
                    {
                        title: 'Water',
                        price: 0.2
                    },
                    {
                        title: 'Butter',
                        price: 1.62
                    },
                    {
                        title: 'Milk',
                        price: 14.42
                    },
                    {
                        title: 'Knife',
                        price: 11.2
                    }
                ];
            }
        };
    }

    angular.module('mainApplication.services').factory('api', apiFunction);
}());