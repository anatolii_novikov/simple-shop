(function () {
    'use strict';

    function controllerFunction($scope, api) {
        var self = this;

        this.goods = api.getGoods();

        $scope.$on('bucketItem:deleteItem', function (event, item) {
            self.goods.push({
                title: item.title,
                price: item.price
            });
        });
    }

    angular.module('mainApplication.controllers')
        .controller('mainCtrl', ['$scope', 'api', controllerFunction]);
}());