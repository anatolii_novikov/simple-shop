(function () {
    'use strict';
    angular.module('simpleShop', [
        'mainApplication',
        'shop'
    ]);
}());